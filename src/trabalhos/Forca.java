/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhos;

import java.util.Scanner;

/**
 *
 * @author XeroX
 */
public class Forca {
    /**
     * 
     * @param String secretWord
     * @param int secretWordLength
     * @param int position
     * @param int livesLost
     * @param int totalLives
     * @param int lettersRemaining
     * @param boolean guessInWord
     * @param char guess;
     * @param args 
     */
    public static void execForca(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    StringBuffer buffer = new StringBuffer();
    
    String secretWord;
    int secretWordLength;
    int position;
    int livesLost = 0;
    int totalLives = 10;
    int lettersRemaining;
    boolean guessInWord;
    char guess;
    StringBuffer prevGuessedLetters;
    
    System.out.println("Enter a word:");
    secretWord = myScanner.next();
    
    //Determinar tamanho
    secretWordLength = secretWord.length();
    System.out.println(secretWordLength);
    lettersRemaining = secretWordLength;
    

    for (position = 0; position < secretWordLength; position++) {
        System.out.print("*");
    }
    System.out.println();
    
    //Loop base
    while (lettersRemaining > 0 && livesLost < 10) {
        //prompt user to guess a letter
        System.out.println("Adivinhe:");
        guess = myScanner.findWithinHorizon(".", 0).charAt(0);

        //check if the letter guessed is in the secretWord  
        guessInWord = (secretWord.indexOf(guess)) != -1;
        if (guessInWord == false) {
            livesLost++;
            System.out.print("Perdeu uma vida");
            System.out.print(totalLives -= livesLost);
            System.out.println(" vidas restantes. tente novamente.");
        } else {
            System.out.println("Acertou");

            for (position = 0; position < secretWordLength; position++) {
                if (secretWord.charAt(position) == guess) {
                    System.out.print(guess);
                    lettersRemaining--;
                } else {
                    System.out.print("*");
                }
            }
        }
                System.out.println();
        prevGuessedLetters = buffer.append(guess);
        System.out.print("Letras inseridas: ");
        System.out.println(prevGuessedLetters);
        System.out.print("Letras restantes: ");
        System.out.println(lettersRemaining);
    }

    if (livesLost == totalLives) {
        System.out.println("Perdeu");
    } else {
        System.out.print("Ganhou, a palavra é: ");
        System.out.println(secretWord);
    }
    
}
}
