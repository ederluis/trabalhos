/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhos;

import java.util.Scanner;

/**
 *
 * @author XeroX
 */
public class JogoGalo {
    /**
 *
 * Verifica se a jogada é valida
 * @param int jogador1 
 * @param int jogador2
 * @param int[][] matriz
 */
    public static int Verificarjogador1(int jogador1, int jogador2, int[][] matriz) {
        int linha;
        int coluna;
        int validarpos;
        int m = 3;
        int n = 3;
        Scanner keyboard = new Scanner(System.in);
        do {
            /**
             *
             * Pede a linha da jogada
             */
            do {
                System.out.println("");
                System.out.println("JOGADOR 1 LINHA");

                linha = keyboard.nextInt();
                if (linha != 0 && linha != 1 && linha != 2) {
                    System.out.println("Opção inválida! Tente novamente");
                }
            } while (linha != 0 && linha != 1 && linha != 2);
            do {
                System.out.println("JOGADOR 1 coluna");

                coluna = keyboard.nextInt();
                if (coluna != 0 && coluna != 1 && coluna != 2) {
                    System.out.println("Opção inválida! Tente novamente");
                }

            } while (coluna != 0 && coluna != 1 && coluna != 2);

            if (matriz[linha][coluna] == jogador2 || matriz[linha][coluna] == jogador1) {
                validarpos = 0;
                System.out.println("Opção inválida! Tente novamente");

            } else {
                validarpos = 1;
                matriz[linha][coluna] = jogador1;
            }
            for (int i = 0; i < m; i++) {

                System.out.println(" ");

                for (int j = 0; j < n; j++) {

                    System.out.print(matriz[i][j]);

                }

            }
            System.out.println("");

            if ((matriz[0][0] == jogador1) && (matriz[0][1] == jogador1) && (matriz[0][2] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }
            if ((matriz[1][0] == jogador1) && (matriz[1][1] == jogador1) && (matriz[1][2] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }

            if ((matriz[2][0] == jogador1) && (matriz[2][1] == jogador1) && (matriz[2][2] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }
            if ((matriz[0][0] == jogador1) && (matriz[1][0] == jogador1) && (matriz[2][0] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }
            if ((matriz[0][1] == jogador1) && (matriz[1][1] == jogador1) && (matriz[2][1] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }

            if ((matriz[0][2] == jogador1) && (matriz[1][2] == jogador1) && (matriz[2][2] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }
            if ((matriz[0][0] == jogador1) && (matriz[1][1] == jogador1) && (matriz[2][2] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }
            if ((matriz[2][0] == jogador1) && (matriz[1][1] == jogador1) && (matriz[0][2] == jogador1)) {
                System.out.println("Jogador 1 ganhou");
                System.exit(0);
            }

        } while (validarpos == 0);

        return 0;
    }
    
     /* Verifica se a jogada do jogador 2 é valida
 * @param int jogador1 
 * @param int jogador2
 * @param int[][] matriz
 */

    public static int Verificarjogador2(int jogador1, int jogador2, int[][] matriz) {
        int linha;
        int coluna;
        int validarpos;
        int m = 3;
        int n = 3;
        Scanner keyboard = new Scanner(System.in);
        do {
            do {
                System.out.println();
                System.out.println("jogador 2 ");
                System.out.println("JOGADOR 2 LINHA");

                linha = keyboard.nextInt();
                if (linha != 0 && linha != 1 && linha != 2) {
                    System.out.println("Opção inválida! Tente novamente");
                }
            } while (linha != 0 && linha != 1 && linha != 2);

            do {
                System.out.println("JOGADOR 2 coluna");

                coluna = keyboard.nextInt();

                if (coluna != 0 && coluna != 1 && coluna != 2) {
                    System.out.println("Opção inválida! Tente novamente");
                }
            } while (coluna != 0 && coluna != 1 && coluna != 2);

            if (matriz[linha][coluna] == jogador2 || matriz[linha][coluna] == jogador1) {
                validarpos = 0;
                System.out.println("Opção inválida! Tente novamente");
            } else {
                validarpos = 1;
                matriz[linha][coluna] = jogador2;
            }
            for (int i = 0; i < m; i++) {

                System.out.println(" ");

                for (int j = 0; j < n; j++) {

                    System.out.print(matriz[i][j]);

                }

            }
            System.out.println("");
            if ((matriz[0][0] == jogador2) && (matriz[0][1] == jogador2) && (matriz[0][2] == jogador2)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }
            if ((matriz[1][0] == jogador2) && (matriz[1][1] == jogador2) && (matriz[1][2] == jogador2)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }

            if ((matriz[2][0] == jogador2) && (matriz[2][1] == jogador2) && (matriz[2][2] == jogador2)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }
            if ((matriz[0][0] == jogador2) && (matriz[1][0] == jogador2) && (matriz[2][0] == jogador2)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }
            if ((matriz[0][1] == jogador2) && (matriz[1][1] == jogador2) && (matriz[2][1] == jogador2)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }

            if ((matriz[0][2] == jogador2) && (matriz[1][2] == jogador2) && (matriz[2][2] == jogador2)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }
            if ((matriz[0][0] == jogador1) && (matriz[1][1] == jogador1) && (matriz[2][2] == jogador1)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }
            if ((matriz[2][0] == jogador1) && (matriz[1][1] == jogador1) && (matriz[0][2] == jogador1)) {
                System.out.println("Jogador 2 ganhou");
                System.exit(0);
            }

        } while (validarpos == 0);

        return 0;
    }
     /* Cria a matriz
 * @param int[][] matriz
 */

    public static int FazerMatriz(int[][] matriz) {
        int m = 3;
        int n = 3;

        for (int i = 0; i < m; i++) {

            System.out.println(" ");

            for (int j = 0; j < n; j++) {

                System.out.print(matriz[i][j]);

            }

        }

        return 0;

    }
     /* 
    * Verifica se a jogada é valida
    *
 */

    public static void execGalo() {
        int jogador1 = 1;
        int jogador2 = 2;
        int m = 3;
        int n = 3;
        int matriz[][] = new int[n][m];
        Scanner keyboard = new Scanner(System.in);
        for (int i = 0; i < m; i++) {

            System.out.println(" ");

            for (int j = 0; j < n; j++) {

                System.out.print("$");

            }

        }
        for (int p = 0; p < 9; p++) {

            int linha;
            int coluna;
            int validarpos = 0;

            int valorjogador1 = Verificarjogador1(jogador1, jogador2, matriz);
            int valorjogador2 = Verificarjogador2(jogador1, jogador2, matriz);

        }
    }
}
