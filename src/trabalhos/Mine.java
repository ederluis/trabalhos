/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhos;

/**
 *
 * @author Eder
 */
public class Mine {
    public static void execMine(String[] args) { 
        int M = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        double p = Double.parseDouble(args[2]);
        
         boolean[][] bombs = new boolean[M+2][N+2];
        for (int i = 1; i <= M; i++){
            for (int j = 1; j <= N; j++){
                bombs[i][j] = (Math.random() < p);
            }
            
        }
        ///imprime
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++)
                if (bombs[i][j]) System.out.print("* ");
                else             System.out.print(". ");
            System.out.println();
        }
        // sol[i][j] = bombs juntas (i, j)
        int[][] sol = new int[M+2][N+2];
        for (int i = 1; i <= M; i++)
            for (int j = 1; j <= N; j++)
                for (int ii = i - 1; ii <= i + 1; ii++)
                    for (int jj = j - 1; jj <= j + 1; jj++)
                        if (bombs[ii][jj]) sol[i][j]++;
        // print solution
        System.out.println();
        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++) {
                if (bombs[i][j]) System.out.print("* ");
                else             System.out.print(sol[i][j] + " ");
            }
            System.out.println();
        }

        
    }
}
