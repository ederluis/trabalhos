/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhos;

import java.util.Scanner;

/**
 *
 * @author XeroX
 */
public class Jogobatalhanaval {

    public static Scanner keyboard = new Scanner(System.in);
    public static String[][] matrizjogador = new String[10][10];
 
/// 
    public static void initVars() {
        String[][] matrizjogador = new String[10][10];
        String[][] matrizcomputador = new String[10][10];
        initMatriz();

        initMatrizcomputador();
    }

    public static String[][] initMatriz() {

        System.out.println("Matriz jogador");
        for (int i = 0; i < matrizjogador.length; i++) {
            for (int j = 0; j < matrizjogador[i].length; j++) {
                matrizjogador[i][j] = ("|_|");
            }
        }

        for (int i = 0; i < matrizjogador.length; i++) {
            System.out.println(" ");
            for (int j = 0; j < matrizjogador[i].length; j++) {

                System.out.print(matrizjogador[i][j]);

            }
            /**
             *
             */
        }
        return matrizjogador;
    }

    public static String[][] initMatrizcomputador() {
        String[][] matrizcomputador = new String[10][10];
        for (int i = 0; i < matrizcomputador.length; i++) {
            for (int j = 0; j < matrizcomputador[i].length; j++) {
                matrizcomputador[i][j] = ("|_|");
            }
        }
        System.out.println("");
        System.out.println("Matriz computador");

        for (int i = 0; i < matrizcomputador.length; i++) {
            System.out.println(" ");
            for (int j = 0; j < matrizcomputador[i].length; j++) {

                System.out.print(matrizcomputador[i][j]);

            }
            /**
             *
             */
        }
        return matrizcomputador;
    }

    public static int[] Introduzirdados() {
        int linha;
        int coluna;
        int orientacao;
        do {
            System.out.println("");
            System.out.println("Introduza a cordenada a coluna do barco");
            coluna = keyboard.nextInt();
            System.out.println("Introduza a cordenada a linha do barco");
            linha = keyboard.nextInt();
        } while (linha > 10 && coluna > 10);
        do {
            System.out.println("Quer o barco em que posição");
            System.out.println("1-Orizontal");
            System.out.println("2-Vertical");

            orientacao = keyboard.nextInt();
        } while (orientacao != 1 && orientacao != 2);
        int[] caracteristicas = null;
        caracteristicas[1] = linha;
        caracteristicas[2] = coluna;
        caracteristicas[3] = orientacao;
        return caracteristicas;

    }

    public static void inserirjogada() {
        int[] caracteristicas = Introduzirdados();
        int linha = caracteristicas[1];
        int coluna = caracteristicas[2];
        int orientacao = caracteristicas[3];

        matrizjogador[linha][coluna] = "|X|";

    }

    public static void imprimirjogada() {
        for (int i = 0; i < matrizjogador.length; i++) {
            System.out.println(" ");
            for (int j = 0; j < matrizjogador[i].length; j++) {

                System.out.print(matrizjogador[i][j]);

            }
        }
    }

    public static void execbatalhanaval() {
        initVars();
        Introduzirdados();
        inserirjogada();
        imprimirjogada();
    }

}
